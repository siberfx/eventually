<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Database\Factories;

use Altek\Eventually\Tests\Models\Award;
use Illuminate\Database\Eloquent\Factories\Factory;

class AwardFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    protected $model = Award::class;

    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->unique()->sentence(2),
        ];
    }
}
